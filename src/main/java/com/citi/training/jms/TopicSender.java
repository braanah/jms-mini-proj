package com.citi.training.jms;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class TopicSender {
	
	@Autowired
	@Qualifier("jmsTopicTemplate")
	JmsTemplate jmsTemplate;
	
//	public void run(ApplicationArguments appArgs) {
//		sendSimpleMessage("We love our dad, ");
//	}
	
	public void sendSimpleMessage(String message) {
		String messageToSend = message + "[" + new Date() + "]";
		System.out.println("Sending message to topic: " + message);
		jmsTemplate.convertAndSend("simpleTopic", messageToSend);
		
	}

}