package com.citi.training.jms;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class TopicListener {
    private static final Logger logger = LoggerFactory.getLogger(TopicListener.class);

    @Bean (name="topicContainerFactory")
    public DefaultJmsListenerContainerFactory topicContainerFactory(ConnectionFactory connectionFactory) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setPubSubDomain(true);
        return factory;
    }

    @Bean(name = "jmsTopicTemplate")
    public JmsTemplate jmsTopicTemplate(ConnectionFactory connectionFactory) throws JMSException {
        JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
        jmsTemplate.setPubSubDomain(true);
        return jmsTemplate;
    }

    @JmsListener(containerFactory = "topicContainerFactory",
                 destination = "simpleTopic")
    public void consumeTrade(Message message) throws JMSException {
        logger.info("JMS topic monitored: {}", message.toString());
        System.out.println("Received from topic: " +
                           ((TextMessage)message).getText());
    }
}
