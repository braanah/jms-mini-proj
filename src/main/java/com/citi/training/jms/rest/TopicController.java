package com.citi.training.jms.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.jms.TopicSender;

@RestController
@RequestMapping("/messages")
public class TopicController {

	@Autowired
	TopicSender topSend;

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public void messagePost(@RequestBody String message) {
		topSend.sendSimpleMessage(message);
	}

}
